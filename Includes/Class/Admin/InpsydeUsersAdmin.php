<?php declare(strict_types=1);
/**
 * The admin specific functionality of the plugin
 *
 * @since       0.1
 * @package     inpsyde_users_plugin
 * @subpackage  inpsyde_users_plugin/admin
 */
namespace InpsydeUsersBuilder;

/**
 * All the actual functions to hook into WordPress
 */
class InpsydeUsersAdmin
{
    /**
     * The ID of this plugin.
     *
     * @since   0.1
     * @access  private
     * @var     string $pluginName The ID of this plugin.
     *
     * @return null
     */
    private $pluginName;
    /**
     * The version of this plugin.
     *
     * @since   0.1
     * @access  private
     * @var     string $version The current version of this plugin.
     *
     * @return null
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since   0.1
     * @access  public
     *
     * @param   string $pluginName The name of this plugin.
     * @param   string $version The version of this plugin.
     *
     * @return null
     */
    public function __construct(string $pluginName, string $version)
    {
        $this->plugin_name = $pluginName;
        $this->version = $version;
    }

    /**
     * Enqueue the Datatable css and js
     * this function is called from builder.class.php::defineAdminHooks
     *
     * @since   0.1
     * @access  public
     */
    public function loadDatatables(): void
    {
        wp_register_script(
            'jquery_datatables_js',
            'https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js',
            [],
            null,
            true
        );
        wp_enqueue_script('jquery_datatables_js');

        wp_register_style(
            'jquery_datatables_css',
            'https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css'
        );
        wp_enqueue_style('jquery_datatables_css');

        /*
        Registers the JS variable of ajaxURL to be used with datatables
        Change the nonce name and nonce string to the plugin name
        */
        $localizeScript = [
            'ajaxURL' => [
                'ajaxURL' => admin_url('admin-ajax.php'),
            ],
            'tableNonce' => wp_create_nonce('inpsyde-users-table-nonce'),
        ];
        wp_localize_script('inpsyde_users_data_tables', 'inpsyde_users_data_tables', $localizeScript);
    }

    /**
     * Enqueue the js file to be used on the front end
     *
     * @since  0.1
     * @access public
     */
    public function loadFrontJs(): void
    {
        wp_register_script(
            'inpsyde_users_js',
            plugins_url('includes/js/front.js', INPSYDE_USERS_FILE),
            [ 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' ]
        );
        wp_enqueue_script('inpsyde_users_js');
    }

    /**
     * Enqueue the css file to be used on the front end
     *
     * @since  0.1
     * @access public
     */
    public function loadFrontCss(): void
    {
        wp_register_style(
            'inpsyde_users_css',
            plugins_url('includes/css/styles.css', INPSYDE_USERS_FILE)
        );
        wp_enqueue_style('inpsyde_users_css');
    }

    /**
     * Adds the endpoint of 'inpsyde_users'
     *
     * @since  0.1
     * @access public
     */
    public function addRoute(): void
    {
        add_rewrite_endpoint(
            'inpsyde_users',
            EP_PAGES
        );
    }

    /**
     * Catches the route and loads the function file
     *
     * @since  0.1
     * @access public
     */
    public function routeCatch(): void
    {
        $path = filter_input(INPUT_SERVER, 'REQUEST_URI');
        //  Only redirect if the path is inpsyde_users.
        if (stripslashes($path) === '/inpsyde_users/') {
            wp_head();
            require_once INPSYDE_USERS_FOLDER . '/includes/route/inpsyde-users.php';
            wp_footer();
            exit(); // we don't want the extra WordPress stuff to load!
        }
    }

    public function createActions(): void
    {
        // Add new route
        add_action(
            'init',
            [ $this,
            'addRoute', ],
        );

        // Add route endpoint catch
        add_action(
            'template_redirect',
            [ $this,
            'routeCatch', ],
        );

        /*
        Enqueue the front end js and styles
        */
        add_action(
            'wp_enqueue_scripts',
            [ $this,
            'loadDatatables', ],
        );
        add_action(
            'wp_enqueue_scripts',
            [ $this,
            'loadFrontJs', ],
        );
        add_action(
            'wp_enqueue_scripts',
            [ $this,
            'loadFrontCss', ],
        );
    }
}
