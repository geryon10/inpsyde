<?php declare(strict_types=1);
/**
 * Plugin Name: inpsyde plugin
 * Description: plugin inpsyde users
 * Version: 0.1
 * Author: Scott Schwab
 * License MIT
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: list
 */
namespace InpsydeUsersBuilder;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Define the base file for enqueuing js files
 * define the base folder for enqueuing php files
 */
if ( ! defined( 'INPSYDE_USERS_FOLDER' ) ) {
	/*
	For enqueueing files into WP
	Use:: wp_register_script( 'inpsyde_admin_main', plugins_url( 'js/admin.main.js', INPSYDE_USERS_FILE ), array( 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' ) );
	*/
	define( 'INPSYDE_USERS_FILE', __FILE__ );

	/*
	For displaying files to the front end
	Use:: $sampleFile = INPSYDE_USERS_URL . '/files/generic.csv';
	*/
	define( 'INPSYDE_USERS_URL', plugins_url( '', __FILE__ ) );

	/*
	For loading files from PHP
	use:: include_once INPSYDE_USERS_FOLDER . "/includes/class/generic_class/controller/table.controller.php";
	*/
	define( 'INPSYDE_USERS_FOLDER', dirname( INPSYDE_USERS_FILE ) );
}

// The object that starts everything!
if ( ! class_exists( 'InpsydeUsersBuilder' ) ) {
	include_once INPSYDE_USERS_FOLDER . '/Includes/Class/Admin/InpsydeUsersBuilder.php';
}

/**
 * Runs the object
 */
function run_inpsyde_builder() {
	$plugin = new InpsydeUsersBuilder();
}
run_inpsyde_builder();
