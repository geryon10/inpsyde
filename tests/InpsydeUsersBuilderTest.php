<?php declare(strict_types=1);
namespace InpsydeUsersBuilder;

use PHPUnit\Framework\TestCase;
use Brain\Monkey;

if (defined('INPSYDE_USERS_FOLDER') === false) {
    define('INPSYDE_USERS_FOLDER', __DIR__ . '/..');
}

/**
 * the test case class
 */
class InpsydeUsersBuilderTest extends TestCase
{
    /**
     * loads the Monkey Brain functionality
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        Monkey\setUp();
    }

    /**
     * test for load dependencies function to return void
     * @return void
     */
    public function testLoadDependenciesIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersBuilder::class);
        $this->assertNull($stub->loadDependencies());
    }

    /**
     * test for define admin hooks function to return void
     * @return void
     */
    public function testDefineAdminHooksIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersBuilder::class);
        $this->assertNull($stub->defineAdminHooks());
    }

    /**
     * test for plugin name function to return string
     * @return void
     */
    public function testPluginNameIsString(): void
    {
        $stub = $this->createMock(InpsydeUsersBuilder::class);
        $this->assertIsString($stub->pluginName());
    }

    /**
     * test for load datatables function to return void
     * @return void
     */
    public function testLoadDataTablesIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersAdmin::class);
        $this->assertNull($stub->loadDatatables());
    }

    /**
     * test for load front JS function to return void
     * @return void
     */
    public function testLoadFrontJsIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersAdmin::class);
        $this->assertNull($stub->loadFrontJs());
    }

    /**
     * test for load front CSS function to return void
     * @return void
     */
    public function testLoadFrontCssIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersAdmin::class);
        $this->assertNull($stub->loadFrontCss());
    }

    /**
     * test for add route function to return void
     * @return void
     */
    public function testAddRouteIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersAdmin::class);
        $this->assertNull($stub->addRoute());
    }

    /**
     * test for route catch function to return void
     * @return void
     */
    public function testRouteCatchIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersAdmin::class);
        $this->assertNull($stub->routeCatch());
    }

    /**
     * test for create action function to return void
     * @return void
     */
    public function testCreateActionsIsVoid(): void
    {
        $stub = $this->createMock(InpsydeUsersAdmin::class);
        $this->assertNull($stub->createActions());
    }

    /**
     * test for admin class to have attributes
     * @return void
     */
    public function testAdminConstructor(): void
    {
        $this->assertClassHasAttribute('pluginName', InpsydeUsersAdmin::class);
        $this->assertClassHasAttribute('version', InpsydeUsersAdmin::class);

        $builder = new InpsydeUsersBuilder();
        $class = new InpsydeUsersAdmin( $builder->pluginName(), $builder->version() );

        $this->assertObjectHasAttribute('pluginName', $class);
        $this->assertObjectHasAttribute('version', $class);
    }

    /**
     * tests that all the appropriate actions have been loaded into WordPress
     * @return void
     */
    public function testAddHooksActuallyAddsHooks(): void
    {
        $builder = new InpsydeUsersBuilder();
        $class = new InpsydeUsersAdmin( $builder->pluginName(), $builder->version() );
        $class->createActions();

        $this->assertTrue( has_action('init', 'InpsydeUsersBuilder\InpsydeUsersAdmin->addRoute()' ) );
        $this->assertTrue( has_action('template_redirect', 'InpsydeUsersBuilder\InpsydeUsersAdmin->routeCatch()' ) );
        $this->assertTrue( has_action('wp_enqueue_scripts', 'InpsydeUsersBuilder\InpsydeUsersAdmin->loadDatatables()' ) );
        $this->assertTrue( has_action('wp_enqueue_scripts', 'InpsydeUsersBuilder\InpsydeUsersAdmin->loadFrontJs()' ) );
        $this->assertTrue( has_action('wp_enqueue_scripts', 'InpsydeUsersBuilder\InpsydeUsersAdmin->loadFrontCss()' ) );
    }
}
