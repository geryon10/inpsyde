# User Lookup #
This plugin creates a endpoint '/inpsyde_users/' which when hit will ajax https://jsonplaceholder.typicode.com/users and display the users in a datatable where the user can then lookup more data on the user. The only outside dependency for this plugin is the jQuery library for datatables. On page load retrieves the json from 'https://jsonplaceholder.typicode.com/users' then adds the base information to the datatable and creates a local array of the users for caching. The table has links to get more data from 'https://jsonplaceholder.typicode.com/posts/' + id of the user, it will then cache that data as well.

The included test are for making sure the builder has a loader and the correct actions that tie into WordPress are correct.

## Table of contents
* [Requirements](#requirements)
* [Setup](#setup)
* [Testing](#testing)

## Requirements
* WordPress 5
* php 7
* datatables jquery library load over cdn

## Setup
To add this plugin to a WordPress install copy the folder into the wp-content/plugins folder.
Activate the plugin from the WordPress admin plugins.

## Testing
To run tests
```
composer install
composer dump-autoload -o
./vendor/bin/phpunit --testdox tests/InpsydeUsersBuilderTest.php
```

### Who do I talk to? ###

* Scott Schwab scschwab@gmail.com