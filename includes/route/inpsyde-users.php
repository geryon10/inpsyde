<?php declare(strict_types=1);
/**
 * The output of the users
 *
 * @since       0.1
 * @package     inpsyde_users_plugin
 */
namespace InpsydeUsersBuilder;

ob_start('ob_gzhandler');
?>
<div class="inpsyde_user_content">
    <div id="inpsyde_user">
        <div id="inpsyde_user_id"></div>
        <div id="inpsyde_user_name"></div>
        <div id="inpsyde_user_username"></div>
        <div id="inpsyde_user_title"></div>
        <div id="inpsyde_user_body"></div>
    </div>
    <div>
        <table id="inpsyde_users">
            <thead>
                <tr>
                <th>id</th>
                <th>Name</th>
                <th>Username</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                <th>id</th>
                <th>Name</th>
                <th>Username</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<?php ob_end_flush(); ?>