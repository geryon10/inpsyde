jQuery(document).ready(function (jQuery) {
    var url = "https://jsonplaceholder.typicode.com/users";

    var arrayUsers = {};

    jQuery.get(url, function ( data ) {
        var usersTable = jQuery("#inpsyde_users").DataTable();    //  use datatables because it makes tables look good

        //  add the rows to the table
        for ( var i = 0; i < data.length; i++ ) {
            linkText = '<a class="load_user" href="' + data[i].id + '" >';

            arrayUsers[data[i].id] = data[i];

            usersTable.row.add([
                linkText + data[i].id + '</a>',
                linkText + data[i].name + '</a>',
                linkText + data[i].username + '</a>'
            ]).draw();
        }
    });

    /**
     * when clicking on the link from the table load more users data
     */
    jQuery("#inpsyde_users").on('click', 'tbody tr td a', function ( e ) {
        e.preventDefault();
        jQuery('.inpsyde_user').css('display', 'none');
        var id = jQuery(this).attr('href'); //  get the id

        //  make sure the id is a integer
        if ( jQuery.isNumeric(id) == false ) {
            alert("You have made an improper selection");
        } else {
            jQuery('.inpsyde_user').css('display', 'none');
            //  load a cached user if it exists
            if ( typeof( arrayUsers[id].title ) != 'undefined' ) {
                jQuery("#inpsyde_user #inpsyde_user_name").html('User: ' + arrayUsers[id].name);
                jQuery("#inpsyde_user #inpsyde_user_id").html('Id: ' + arrayUsers[id].id);
                jQuery("#inpsyde_user #inpsyde_user_title").html('Title: ' + arrayUsers[id].title);
                jQuery("#inpsyde_user #inpsyde_user_body").html('<p>' + arrayUsers[id].body + '</p>');
                jQuery("#inpsyde_user #inpsyde_user_username").html('Username: ' + arrayUsers[id].username);
            } else {
                //  else get the information
                //  get the detail of the selected user
                var userUrl = "https://jsonplaceholder.typicode.com/posts/" + id;

                jQuery.get(userUrl, function ( data ) {
                    //  client side cache
                    arrayUsers[id].title = data.title;
                    arrayUsers[id].body = data.body;

                    jQuery("#inpsyde_user #inpsyde_user_name").html('User: ' + arrayUsers[id].name);
                    jQuery("#inpsyde_user #inpsyde_user_username").html('Username: ' + arrayUsers[id].username);
                    jQuery("#inpsyde_user #inpsyde_user_id").html('Id: ' + id);
                    jQuery("#inpsyde_user #inpsyde_user_title").html('Title: ' + arrayUsers[id].title);
                    jQuery("#inpsyde_user #inpsyde_user_body").html('<p>' + arrayUsers[id].body + '</p>');
                });
            }
            jQuery('.inpsyde_user').css('display', 'block');
        }
    });
});